#https://www.tensorflow.org/tutorials/
#https://keras.io/layers/convolutional/
#https://mohitatgithub.github.io/2018-03-28-MNIST-Image-Classification-with-CNN-&-Keras/
#http://neuralnetworksanddeeplearning.com/chap6.html
import tensorflow as tf

mnist = tf.keras.datasets.mnist

(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)
input_shape = (28, 28, 1)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train, x_test = x_train / 255.0, x_test / 255.0

num_of_filters=1
num_of_neurons=41 #fc layer


model = tf.keras.models.Sequential([
  tf.keras.layers.Conv2D(filters = num_of_filters, kernel_size= (5,5),padding='valid', activation='relu', input_shape=input_shape), # output 24x24
  tf.keras.layers.MaxPooling2D(pool_size=3),
  tf.keras.layers.Flatten(),
  tf.keras.layers.Dense(num_of_neurons, activation=tf.nn.relu),# tf.keras.activations.linear
  tf.keras.layers.Dense(10, activation=tf.nn.softmax)
])
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x_train, y_train, epochs=5)
test_loss_acc=model.evaluate(x_test, y_test)
wt = model.get_weights()
### Probabilities and classes of the all images from the test data
probabilities=model.predict(x_test, batch_size=32, verbose=0)
labels=model.predict_classes(x_test, batch_size=32, verbose=0)
### Exporting loss and accuracy
f=open('loss_and_acc.txt','w')
f.write('Loss: %f\n'%test_loss_acc[0])
f.write('Accuracy: %f\n'%test_loss_acc[1])
f.close()
### Exporting weights
f=open('weights.h','w')
f.write('//Conv bias Rows=5, Columns=5, Depth= %d\n#define WTs_C {'%num_of_filters)
i = 0
j = 0
k = 0        
for k in range(num_of_filters):
    f.write('{') 
    for i in range(5):
        f.write('{') 
        for j in range(5):
            if j == 4:
                f.write('%f' % wt[0][i][j][0][k]+'}')
            else:
                f.write('%f' % wt[0][i][j][0][k]+',')
        if i == 4:
            f.write('}')
        else:
            f.write(',')
    if k == num_of_filters-1:
        f.write('}\n\n')
    else:
        f.write(',')
f.write('//Conv bias Rows=%d, Columns=1\n#define BIAS_C {'%num_of_filters)
j=0
for j in range(num_of_filters-1):
    f.write('%f' % wt[1][j]+', ')
f.write('%f' % wt[1][num_of_filters-1])
f.write('}\n\n')      
f.write('//Conv: Rows=64*%d'%num_of_filters+', Columns=%d\n#define WTs_FC {'%num_of_neurons)
i=0
j=0
for i in range(64*num_of_filters):
    for j in range(num_of_neurons-1):
        f.write('%f' % wt[2][i][j]+', ')
    if i==64*num_of_filters-1:
        f.write('%f' % wt[2][i][num_of_neurons-1])
    else:
        f.write('%f' % wt[2][i][num_of_neurons-1]+', ')
f.write('}\n\n')
f.write('//FC bias Rows=%d, Columns=1\n#define BIAS_FC {'%num_of_neurons)
j=0
for j in range(num_of_neurons-1):
    f.write('%f' % wt[3][j]+', ')
f.write('%f' % wt[3][num_of_neurons-1])
f.write('}\n\n')
f.write('//Softmax: Rows=%d, Columns=10\n#define WTs_S {'%num_of_neurons)
i=0
j=0
for i in range(num_of_neurons):
    for j in range(9):
        f.write('%f' % wt[4][i][j]+', ')
    if i==num_of_neurons-1:
        f.write('%f' % wt[4][i][9])
    else:
        f.write('%f' % wt[4][i][9]+', ')
f.write('}\n\n')
f.write('//Softmax bias Rows=10, Columns=1\n#define BIAS_S {')
j=0
for j in range(9):
    f.write('%f' % wt[5][j]+', ')
f.write('%f' % wt[5][9])
f.write('}\n\n')
f.close()
###image data generator
shift = 0;
l = 0
for l in range(10):
    img = 0
    while y_test[img+shift]!=l:
        img+=1
    test_img = x_test[img+shift]
    f=open('image'+'%d' % l+'.txt','w')
    i=0
    j=0
    for i in range(28):
        for j in range(28):
            f.write('%d' % int(test_img[i][j]*255)+',')
       # f.write('\n')
    f.write('\n\nProbabilities:')
    i = 0
    for i in range(10):
        f.write(' %.4f'%probabilities[img+shift][i])
    f.close()
###Show output of the particular layer
get_layer_output = tf.keras.backend.function([model.input],
                                             [model.layers[3].output])
img=0
while y_test[img+shift]!=0:
    img+=1
output_conv=get_layer_output([x_test])[0][img+shift]
